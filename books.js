// const books = require('./data/data.json')
const fs = require('fs')
// class Book{
//     static records = books;

//     constructor(params){
//         this.title = params.title;
//         this.description = params.description;
//         this.price = params.price
//     }

//     update(params){
//         params.title && (this.title = params.title);
//         params.description && (this.description = params.description);
//         params.price && (this.price = params.price);
//     }



// }

const loadBooks = () =>{
    const fileBuffer = fs.readFileSync('./data/data.json','utf-8')
    const books = JSON.parse(fileBuffer)
    return books
}

//write data.json with new files
const saveBooks = (books)=>{
    fs.writeFileSync('./data/data.json', JSON.stringify(books))
}

//add data.json
const addBooks = (book) =>{
    const books = loadBooks();
    books.push(book);
    saveBooks(books)
}

module.exports = {loadBooks, saveBooks, addBooks}