const express = require('express');
const app = express();
const path = require('path')
const PORT = 8000;
const fs = require('fs')
const {loadBooks, saveBooks, addBooks} = require('./books')


const PUB_PATH = path.join(__dirname, 'public')

app.set('view engine','ejs')
app.use(express.static(PUB_PATH))
app.use(express.json())
app.use(express.urlencoded())


app.get('/', (req,res)=>{
    const listBooks = loadBooks()
    res.render('index', {title : 'Homepage', data: listBooks})
})

app.get('/buy', (req, res) =>{
    res.render('buy', {title : 'Buy'})
})

app.get('/register',(req, res)=>{
    res.render('register', {title : 'Register'})
})

app.get('/add', (req, res) =>{
    res.render('add', {
        title : 'Add Books'
    })
})

app.post('/index', (req, res) =>{
    addBooks(req.body)
    res.redirect('/')
})


// app.use((req, res, next) =>{
//     res.send('error')
//     next()
// })

app.listen(PORT, ()=>{
    console.log(`Server on! listening on Port ${PORT}, go to http://localhost:${PORT}`)
})

